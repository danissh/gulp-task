# Gulp task tut for Aziz

## Selam Aziz

In order to get you stated with gulp you should have node installed and also npm. Simply verify this by going to terminal and write following:

`node -v`
`npm -v`

Now, As you everything installed lets get started. Wherever you would like to do Css Minification and JS minification. I wrote Gulpfile script already for you as well as package.json. You don't need node_modules folder. just take these 2 files to any project and do following,

`npm install`

if it throws error then make you have gulp installed globally in the system using `npm install -g gulp`

This command will install all necessary requirements for you regarding gulp task.


# what gulp is doing
If you open gulpfile you will see it import gulp and gulp plugins to do css and js tasks.
for more info see this (https://semaphoreci.com/community/tutorials/getting-started-with-gulp-js)[https://semaphoreci.com/community/tutorials/getting-started-with-gulp-js]


# Project setup

```
├── assets					# Created this with scripts and styles folder in it.
|        ├── scripts
|		 |	└── main.js
|		 └── styles
|   		└── main.scss
├── gulpfile.js
├── index.html
├── package.json
├── build                   # Compiled files (alternatively `dist`) Complied and Created Automatically
```

Necessary structure here is that you should have assets folder with scripts and styles folder indvisually.

# Running

In order to watch and compile Realtime changes of css and js, Run following

`gulp watch` 


but if you want to minify stuff at the end of project, means no realtime compilation then, Run following

`gulp`


Both command will create folder called Build with minifed styles and scripts in there indvisual respective folders.


Cheers!


