var self = this;
/*
 * @usage 
 * `var el = document.getElementById('peace')
 *    addClass(el, 'be-still')
 *    // or
 *    removeClass(el, 'be-still')
 *  `
 */
// check for hasclass
function hasClass(el, className) {
    if (el.classList) {
        return el.classList.contains(className)
    }
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}
// to add class to el
function addClass(el, className) {
    if (el.classList)
        el.classList.add(className)
    else if (!hasClass(el, className)) el.className += " " + className
}
// to remove class from el
function removeClass(el, className) {
    if (el.classList)
        el.classList.remove(className)
    else if (hasClass(el, className)) {
        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
        el.className = el.className.replace(reg, ' ')
    }
}

function test() {
	console.log('danish');
}